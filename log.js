const fs = require('fs')
module.exports = { log: (m,cb,skip=false) => {                               
  const text = m.content                                                      
  const chan = m.channel.name                                                 
  const user = m.author.username                                              
  const msg  = `#${chan}@${user}: ${text}\n`
  if (skip) return                                               
  print(msg)                                                                  
  fs.appendFile('log', msg+'\n', e => e && (print(`<LOG ERROR>\n${msg}\n${e}`) || e))
  cb && cb()
}}
