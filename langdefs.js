const {L,D,T,J,S} = require('./helpers')
const langdata = tildir + '/aplfarm-languages/langdata.json'
const fs = require('fs')
const kplname = prod ? 'ubuntu' : 'ndr'
const languages   = () => {
  const l = JSON.parse(fs.readFileSync(langdata,'utf-8'))
  return ({
    // REQUIRED: cmd, dir
    // OPTIONAL: ignoredChannels, mod(code), fmt(response)
    ...l, // bqn, j, apl
    j: {
      ...l.j,
      mode: code => `unused =: 9!:25]1` + '\n' +  code + '\n'
    },
    dyalog: {
      ...l.dyalog,
      mod: code => 'main←{⎕IO←1\n' + code + '\n}',
      fmt: code => print(L(code)) || code,
      dir: prod ? 'ubuntu' : 'ndr'
    },
    nk : { 
      mod: code => code + '\n', // ngn files must terminate in LF
      ...l.nk  },
    april : {
      mod: code => `main←{\n${code}\n}`,
      fmt: code => J('',T(-1,S('@@@@@@\n',code))),
      ...l.april
    },
    kpl : {
       mod: code => '\\l /home/' + kplname + '/kpl/apl.k\n' + code.trim() + '\n\n',
       ...l.nk },
    kona: { 
      fmt: resp => J('\n',D(-1,S('\n',resp))), // drop last LF in output
      ...l.kona },
    h: { 
      mod: code => { 
        // Take inline arguments, then 
        // use the language list to return documentation
        const names = Object.keys(l).map(x => `${x}\)`)
        return 'Available language commands: \n' +  names.join('\n')
      }, ...l.h }
  })
}
module.exports = { languages }
