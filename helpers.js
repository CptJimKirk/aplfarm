const L =    A  => A.length                               //≢
const D = (n,A) => n>0 ? A.slice(n)  : A.slice(0, L(A)+n) //↓
const T = (n,A) => n>0 ? A.slice(0,n): A.slice(n)         //↑
const J = (c,A) => A.join(c)                              //join
const S = (c,A) => A.split(c)                             //split
module.exports = { L,D,T,J,S }
