const {L,D,T,J,S} = require('./helpers')
const matrixat = (m,ts) => {
  const [_,user] = S('@',ts[0])
  m.reply(`https://matrix.to/#/@${user}:matrix.org`)
}

const matrixatMention = (m,ts) => { // mentions a discord user
  const user = m.mentions.users.first().username
  m.reply(`https://matrix.to/#/@${user}:matrix.org`)
}

const commands = {
  "!m@": matrixat,
  "!m<@": matrixatMention 
}
const Command = (m,ts) => {
    Object.keys(commands).forEach(c => {
      if (ts[0].includes(c)) commands[c](m,ts)
    })
}

module.exports = { Command }
