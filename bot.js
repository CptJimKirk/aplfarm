const os          = require('os')
global.prod       = process.env.machine === 'prod'
global.tildir     = os.homedir()
global.print      = console.log
const {cleanup}   = require('./cleanup.js')
const {L,D,T,J,S} = require('./helpers')
const {Command}   = require('./commands')
const {Execute}   = require('./languages')
const discord     = require('discord.js')

const d      = new discord.Client()
const tok    = prod ? process.env.prodbot : process.env.devbot
const do_all = m => {
  if(!prod && !(m.channel.name === 'test-dev'))  return
  const text = m.content
  const ts = S(' ',text)  // text split on space
  if (L(ts) === 0) return // if no results, stop
  Command(m,ts)           // process general cmds
  print(m.content)
  Execute(m)              // execute expressions
}

d.on('ready'        ,  _               => print(`${d.user.tag} connected!`))
d.on('message'      ,  m               => do_all(m))
d.on('messageUpdate', (original, edit) => do_all(edit))
d.login(tok)
