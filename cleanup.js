const cleanup = bot => {
  const ppid  = '877703698707513354'
  const print = m =>  console.log(m)
  bot.channels.fetch(ppid, {allowUnknownGuild: true}).then(chan => {
    let   sent = false
    const prod = process.env.machine === 'prod'
    const end  = code => () => {
      if (sent) return
      sent = true
      print('the end')
      prod && chan.send('Language bot offline')
      code && process.exit(code)
    }
    process.on('uncaughtException', end(99))
    process.on('exit',  end())
    process.on('SIGINT',  end(2))
    process.on('SIGTERM',  end())

    prod && chan.send('Language bot online')
  })
}
module.exports = { cleanup }
