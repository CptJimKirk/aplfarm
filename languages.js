const {L,D,T,J,S} = require('./helpers')
const {languages} = require('./langdefs.js')
const {exec}      = require('child_process')
const {log}       = require('./log.js')
const template    = require('string-interp')
const fs          = require('fs')

const respond = (m, r) => {
  const fmt  = t => J('\n',T(10, S('\n',t).map(r => T(100, r))))
  const resp = '```\n'+fmt(r)+'\n```'
  const user = m.author.username
  const matr = `https://matrix.to/#/@${user}:matrix.org`
  print('responding')
  if (m.author.bot) m.channel.send(`${matr}\n${resp}`)
  else m.reply(resp)
}

const run = (m,cmd,fmt) => {
  print('running')
  
  if (!cmd) respond(m, "This language is not available yet")
  const proc = exec(cmd, {timeout: 10000},(err, stdout,stderr) => {
    if(err){
      print('error',err)
      if (proc) proc.kill('SIGKILL')
      if (err.killed) respond(m, 'APLFARM: Evaluation Timed Out')
      respond(m, 'There was an error executing that expression.')
    }
    if(stderr) print('stderr',stderr)
    if(!err && !stderr) respond(m,fmt ? fmt(stdout) : stdout)
  })
}

const write = (m, filename,code) => {
  const buf = new Buffer.from(code)
  fs.open(filename, 'w', (e, f) => {
    if (e) print(e)
    fs.write(f, buf, 0, L(buf), null, err => {
      if(err) {
        print('error writing file', err)
        return
      }
      fs.close(f, () => print('file written'))
    })
  })
}

const call = (m,code,lang) => {
  // set up the call to run the command with all necessary arguments
  const l = languages()[lang]
  const {
    ignoreChannels=[], // do not run command from these
    dir,               // install directory of language
    cmd,               // command to be executed
    mod=undefined,     // modify the code in some way
    fmt=undefined,     // format the output
  } = l

  if (ignoreChannels.includes(m.channel.name)) return 
  const c = mod ? mod(code) : code       
  const f = './tmp/' + (lang + 'tmp')
  write(m, f, c)
  run(m, template(cmd, {d:dir,f}),fmt)
}

const eval = (m,s) => {
  // take 3 of s or '' which is undefined, thus no op
  const [l,e] = D(1,T(3,s || '')) 
  log(m, () => call(m,e,l), !(l in languages()))
}

const rm         = re => m => m.match(re)
const expr       = rm(/`(.+?)\)(.+?)`/)             
const multiline  = rm(/```.*?\n?(.+?)\)\n?([\s\S]+)\n?```/)
const inline     = rm(/^(.+?)\)(.*?)$/)
const multinline = rm(/^(?!```.+?\n)(.*?)\)\n([\s\S]+)/)  
const Execute    = m => parsers.map(f => eval(m,f(m.content)))
const parsers    = [inline,multiline,expr,multinline]
module.exports   = {Execute}

//const helpcmd    = m => m.match(/^`?(.+?)\)`?\w*$/) // match only "word)"

